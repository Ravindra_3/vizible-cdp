import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FetchTableDataService {
  private _url: string = '/assets/data.json';

  constructor(private http: HttpClient) {}

  gettagdata() {
    const url = 'https://cdptestapi.datawrkz.com/tags';
    return this.http.get<any>(url).pipe();
  }

  addtagdata(data): Observable<any> {
    const url = 'https://cdptestapi.datawrkz.com/tags/';

    return this.http.post<any>(url, data).pipe();
  }

  private _url1: string = '/assets/Macros.json';
  getmacrosdata() {
    return this.http.get<any>(this._url1).pipe();
  }

  getTagDetails(id) {
    const url = `https://cdptestapi.datawrkz.com/tags/${id}`;

    return this.http.get<any>(url).pipe();
  }

  updateTagDetail(id, data): Observable<any> {
    const url = `https://cdptestapi.datawrkz.com/tags/${id}`;

    return this.http.put<any>(url, data).pipe();
  }
  deleteTag(id) {
    const url = `https://cdptestapi.datawrkz.com/tags/${id}/`;
    return this.http.delete(url).pipe();
  }
}
