import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { FetchTableDataService } from './fetch-table-data.service';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private _injector: Injector) {}

  intercept(req, next) {
    let fetchservies = this._injector.get(FetchTableDataService);

    let tokennizedreq = req.clone({
      setHeaders: {
        'Content-Type': 'application/json',
        'Authorization': 'Token aa7acdd5b7f893b6bbc08648af52e24bd09d1b50',
      },
    });
    return next.handle(tokennizedreq);
  }
}
