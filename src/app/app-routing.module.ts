import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { NewEdit1Component } from './tag/EditTag/new-edit1.component';
import { NewCreateTag1Component } from './tag/new-create-tag1/new-create-tag1.component';
import { TableComponent } from './tag/table/table.component';

const routes: Routes = [
  { path: 'tag', component: TableComponent },
  { path: 'tag/create', component: NewCreateTag1Component },
  { path: 'tag/:id', component: NewEdit1Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
