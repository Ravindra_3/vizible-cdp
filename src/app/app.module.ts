import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FetchTableDataService } from './apiServieces/fetch-table-data.service';
import { HttpClientModule ,HTTP_INTERCEPTORS} from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { NewEdit1Component } from './tag/EditTag/new-edit1.component';
import { NewCreateTag1Component } from './tag/new-create-tag1/new-create-tag1.component';
import { TableComponent } from './tag/table/table.component';
import {TokenInterceptorService} from './apiServieces/token-interceptor.service';
import { TagdetailFormComponent } from './common/tagdetail-form/tagdetail-form.component'
import {NgxPaginationModule} from 'ngx-pagination'

@NgModule({
  declarations: [
    AppComponent,
    NewEdit1Component,
    NewCreateTag1Component,
    TableComponent,
    TagdetailFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    MatCardModule,
    NgxPaginationModule
  ],
  providers: [FetchTableDataService,{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true
  }],
  bootstrap: [AppComponent],
})
export class AppModule {}
