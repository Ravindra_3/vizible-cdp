import { Component,EventEmitter, OnInit,Input ,Output  } from '@angular/core';
import { FetchTableDataService } from '../../apiServieces/fetch-table-data.service';
import { Router } from '@angular/router';
import { FormGroup} from '@angular/forms';
// import {form}
@Component({
  selector: 'app-tagdetail-form',
  templateUrl: './tagdetail-form.component.html',
  styleUrls: ['./tagdetail-form.component.css']
})
export class TagdetailFormComponent implements OnInit {
  public panelOpenState = false;
  public checked;
  public numberofpanel = [1];
  public numberofmacrospanel = [1];
  public showtrigerbulde = true;
  public showmemberidinput = false;
  public Macros;

  public selectdata='Javascript'
  public state1;
  public plateform=1
  // public name=''
  public url=''
  public memberid;
  public category=''
  // public operator;
  // public value=''
  public name_triger=''
  public segment_id;
  public sync=''

 
  constructor(
    private _dataServies: FetchTableDataService,
    private router: Router
  ) {}
  @Input('name') name:any
  @Input('operator') operator:any
  @Input('value') value:any
  @Output() test = new EventEmitter();

  ngOnInit() {
    this._dataServies.getmacrosdata().subscribe((data) => {
      console.log(data);
      this.Macros = data;
    });
  }
  extentendpanel(event) {
    this.numberofpanel.push(
      this.numberofpanel[this.numberofpanel.length - 1] + 1
    );
  }
  deleteextentendpanel($event, index) {
    const i = parseInt(index);
    if (this.numberofpanel.length > 1) {
      const a = this.numberofpanel.splice(i, 1);
    }
  }
  hideshowtrigerbulde(event) {
    if (event.target.value == 'Image') {
      this.showtrigerbulde = false;
    } else {
      this.showtrigerbulde = true;
    }
    this.selectdata = event.target.value;
  }
  dismemberidinput(event) {
    if (event.target.value == '2') {
      this.showmemberidinput = true;
    } else {
      this.showmemberidinput = false;
    }
    this.plateform=event.target.value
    console.log(this.plateform)
  }

  extendcustommacros(event) {
    this.numberofmacrospanel.push(
      this.numberofmacrospanel[this.numberofmacrospanel.length - 1] + 1
    );
  }
  delextendcustommacros(event, index) {
    const i = parseInt(index);
    if (this.numberofmacrospanel.length > 1) {
      const a = this.numberofmacrospanel.splice(i, 1);
    }
  }
  gotohomepage() {
    this.router.navigate(['tag']);
  }
  onSubmit(){
  //   const Data={
  //     "platform": 1,
  //     "name": "Tag test 207",
  //     "landing_page": "www.abcd.com",
  //     "type": "Javascript",
  //     "custom_text": "",
  //     "image_text": "",
  //     "advertiser_id": 159632,
  //     "dsp_sync_flag": true,
  //     "tag_url": "abc.com",
  //     "tag_attributes": [
  //        {
  //            "operator": 1,
  //            "segment_id": 111222,
  //            "dsp_sync_flag": true,
  //            "value": "text88",
  //            "event": 21,
  //            "custom_event_name": "TEST_B1",
  //            "attribute_event_name": ""
  //        }]
        
  // }


    const Data={
      "platform": this.plateform,
      "name": this.name,
      "landing_page": this.url,
      "type": this.selectdata,
      "custom_text": "",
      "image_text": "",
      "advertiser_id": 159632,
      "dsp_sync_flag": this.state1,
      "tag_url": "",
      "tag_attributes":[       {
        "operator": this.operator,
        "segment_id": this.segment_id,
        "dsp_sync_flag": this.sync,
        "value":this.value,
        "event": 21,
        "custom_event_name": this.name_triger,
        "attribute_event_name": ""
    }]


    
    }
    // console.log(Data)
    // console.log(this.operator)
    this._dataServies.addtagdata(Data).subscribe((data) => {
      if(data!=null){
        this.gotohomepage()
      }
     
    })

  }
  clearform(form: FormGroup) {
    form.reset();
  }

  test1(e){
    this.test.emit(e)
    console.log("child")

  }

  // selectcategory(event) {
  //   console.log(event.target.value);
  // }
  // selectoperater(event) {
  //   // (event.target.value)
  //   this.operator = event.target.value;
  // }

}
