import { Component, OnInit } from '@angular/core';
import { FetchTableDataService } from '../../apiServieces/fetch-table-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { error } from 'protractor';
// import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'app-new-edit1',
  templateUrl: './new-edit1.component.html',
  styleUrls: ['./new-edit1.component.css'],
})
export class NewEdit1Component implements OnInit {
  public panelOpenState = false;
  public checked;
  public numberofpanel = [1];
  public numberofmacrospanel = [1];
  public showtrigerbulde = true;
  public showmemberidinput = false;
  public Macros;
  public tagId;
  public TagDetails;

  public selectdata = 'Javascript';
  public state1 = true;
  public plateform = 2;
  public name = 'ravindra';
  public url = '';
  public memberid;
  public category = '';
  public operator;
  public value = '';
  public name_triger = '';
  public segment_id;
  public sync = '';

  constructor(
    private _dataServies: FetchTableDataService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tagId = this.route.snapshot.paramMap.get('id');
    console.log(this.tagId);

    this._dataServies.getTagDetails(this.tagId).subscribe(
      (data) => {
        // console.log(data);
        this.TagDetails = data;
        this.name = data['name'];
        this.url = data['website'];
        this.state1 = data['is_active'];
        this.selectdata = data['type'];
        this.plateform = data['platform'];
        this.operator = data['tag_attributes'][0]['operator'];
        this.value = data['tag_attributes'][0]['value'];
        this.name_triger = data['tag_attributes'][0]['custom_event_name'];
        this.segment_id = data['tag_attributes'][0]['segment_id'];
        this.sync = data['is_synced'];
      },

      (error) => console.log(error)
    );

    this._dataServies.getmacrosdata().subscribe((data) => {

      this.Macros = data;
    },
    (error) => console.log(error));
  }
  extentendpanel(event) {
    this.numberofpanel.push(
      this.numberofpanel[this.numberofpanel.length - 1] + 1
    );
  }
  deleteextentendpanel($event, index) {
    const i = parseInt(index);
    if (this.numberofpanel.length > 1) {
      const a = this.numberofpanel.splice(i, 1);
    }
  }
  hideshowtrigerbulde(event) {
    if (event.target.value == 'Image') {
      this.showtrigerbulde = false;
    } else {
      this.showtrigerbulde = true;
    }
  }
  dismemberidinput(event) {
    console.log(event.target.value);
    if (event.target.value == 'appenexus') {
      this.showmemberidinput = true;
    } else {
      this.showmemberidinput = false;
    }
  }

  extendcustommacros(event) {
    this.numberofmacrospanel.push(
      this.numberofmacrospanel[this.numberofmacrospanel.length - 1] + 1
    );
  }
  delextendcustommacros(event, index) {
    const i = parseInt(index);
    if (this.numberofmacrospanel.length > 1) {
      const a = this.numberofmacrospanel.splice(i, 1);
    }
  }
  gotohomepage() {
    this.router.navigate(['tag']);
  }
  clearform(form: FormGroup) {
    form.reset();
  }

  selectcategory(event) {
    console.log(event.target.value);
  }
  selectoperater(event) {
    // (event.target.value)
    this.operator = event.target.value;
  }
  onSubmit() {
    const Data = {
      platform: this.plateform,
      name: this.name,
      landing_page: this.url,
      type: this.selectdata,
      custom_text: '',
      image_text: '',
      advertiser_id: 159632,
      dsp_sync_flag: this.state1,
      tag_url: '',
      tag_attributes: [
        {
          operator: this.operator,
          segment_id: this.segment_id,
          dsp_sync_flag: this.sync,
          value: this.value,
          event: 21,
          custom_event_name: this.name_triger,
          attribute_event_name: '',
        },
      ],
    };

    // console.log(Data);
    this._dataServies.updateTagDetail(this.tagId, Data).subscribe((data) => {
      if (data != null) {
        this.router.navigate(['tag']);
      }
    },
    (error) => console.log(error));
  }
  test(event){
    console.log(event)
  }
}
